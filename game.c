#include "copyright.h"

#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
    
#include "db.h"
#include "config.h"
#include "interface.h"
#include "match.h"
#include "externs.h"

/* declarations */
static const char *dumpfile = 0;
static int epoch = 0;
static int alarm_triggered = 0;
static int alarm_block = 0;

static void fork_and_dump(void);
void dump_database(void);

void do_tune(dbref player, const char *varname, const char *valstr);

void do_dump(dbref player)
{
    if(Wizard(player)) {
	alarm_triggered = 1;
	notify(player, "Dumping...");
    } else {
	notify(player, "Sorry, you are in a no dumping zone.");
    }
}

void do_shutdown(dbref player)
{
    if(Wizard(player)) {
	writelog("SHUTDOWN: by %s(%d)\n", db[player].name, player);
	fflush(stderr);
	shutdown_flag = 1;
    } else {
	notify(player, "Your delusions of grandeur have been duly noted.");
    }
}
	
/* should be void, but it's defined as int */
static int alarm_handler(void)
{
    alarm_triggered = 1;
    if(!alarm_block) {
        fork_and_dump();
    }
#ifdef IRIX
    signal(SIGALRM, (void (*)) alarm_handler);
    signal(SIGHUP, (void (*)) alarm_handler);
#endif
    return 0;
}

static void dump_database_internal(void)
{
    char tmpfile[2048];
    FILE *f;

    sprintf(tmpfile, "%s.#%d#", dumpfile, epoch - 1);
    unlink(tmpfile);		/* nuke our predecessor */

    sprintf(tmpfile, "%s.#%d#", dumpfile, epoch);

    if((f = fopen(tmpfile, "w")) != NULL) {
	db_write(f);
	fclose(f);
	if(rename(tmpfile, dumpfile) < 0) perror(tmpfile);
    } else {
	perror(tmpfile);
    }
}

void panic(const char *message)
{
    char panicfile[2048];
    FILE *f;
    int i;

    writelog("PANIC: %s\n", message);

    /* turn off signals */
    for(i = 0; i < NSIG; i++) {
	signal(i, SIG_IGN);
    }

    /* shut down interface */
    emergency_shutdown();

    /* dump panic file */
    sprintf(panicfile, "%s.PANIC", dumpfile);
    if((f = fopen(panicfile, "w")) == NULL) {
	perror("CANNOT OPEN PANIC FILE, YOU LOSE:");
#ifndef NODUMPCORE
	signal(SIGILL, SIG_DFL);
	abort();
#endif /* NODUMPCORE */
	_exit(135);
    } else {
	writelog("DUMPING: %s\n", panicfile);
	db_write(f);
	fclose(f);
	writelog("DUMPING: %s (done)\n", panicfile);
#ifndef NODUMPCORE
	signal(SIGILL, SIG_DFL);
	abort();
#endif /* NODUMPCORE */
	_exit(136);
    }
}

void writelog(const char *fmt, ...)
{
    va_list list;
    struct tm *tm;
    time_t t;
    char buffer[2048];

    va_start(list, fmt);
    vsprintf(buffer, fmt, list);
    t = time(NULL);
    tm = localtime(&t);
    fprintf(stderr, "%d/%02d %02d:%02d:%02d %s",
	    tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
	    buffer);
    fflush(stderr);
    va_end(list);
}

void dump_database(void)
{
    epoch++;

    writelog("DUMPING: %s.#%d#\n", dumpfile, epoch);
    dump_database_internal();
    writelog("DUMPING: %s.#%d# (done)\n", dumpfile, epoch);
}

static void fork_and_dump(void)
{
    int child;

    epoch++;

    writelog("CHECKPOINTING: %s.#%d#\n", dumpfile, epoch);
#ifdef USE_VFORK
    child = vfork();
#else /* USE_VFORK */
    child = fork();
#endif /* USE_VFORK */
    if(child == 0) {
	/* in the child */
	close(0);		/* get that file descriptor back */
	dump_database_internal();
	_exit(0);		/* !!! */
    } else if(child < 0) {
	perror("fork_and_dump: fork()");
    }
	
    /* in the parent */
    /* reset alarm */
    alarm_triggered = 0;
    alarm(DUMP_INTERVAL);
}

static int reaper(void)
{
    int stat;

    while(wait3(&stat, WNOHANG, 0) > 0);
#ifdef IRIX
    signal(SIGCHLD, (void (*)) reaper);
#endif
    return 0;
}

int init_game(const char *infile, const char *outfile)
{
   FILE *f;

   if((f = fopen(infile, "r")) == NULL) return -1;
   
   /* ok, read it in */
   writelog("LOADING: %s\n", infile);
   if(db_read(f) < 0) return -1;
   writelog("LOADING: %s (done)\n", infile);

   /* everything ok */
   fclose(f);

   /* initialize random number generator */
   srandom(getpid());

   /* set up dumper */
   if(dumpfile) free_mem((void *) dumpfile);
   dumpfile = alloc_string(outfile);
   signal(SIGALRM, (void (*)) alarm_handler);
   signal(SIGHUP, (void (*)) alarm_handler);
   signal(SIGCHLD, (void (*)) reaper);
   alarm_triggered = 0;
   alarm(DUMP_INTERVAL);
   
   return 0;
}

/* use this only in process_command */
#define Matched(cmd, min) \
	else if(string_precmp(command, (cmd)) >= (min))

void process_command(dbref player, char *command)
{
  char *arg1, *arg2;
  char *p, *q;		/* utility */

  if(command == 0) abort();

  /* robustify player */
  if(player < 0 || player >= db_top || Typeof(player) != TYPE_PLAYER) {
    writelog("process_command: bad player %d\n", player);
    return;
  }

#ifdef LOG_COMMANDS
  writelog("COMMAND from %s(%d) in %s(%d): %s\n",
	   db[player].name, player,
	   db[db[player].location].name, db[player].location,
	   command);
#endif /* LOG_COMMANDS */
    
#ifdef TIMESTAMPS
  db[player].usecnt++;
  db[player].lastused = time (0);
#endif /* TIMESTAMPS */

  /* eat leading whitespace */
  while(*command && isspace(*command)) command++;

  /* eat extra white space */
  q = p = command;
  while(*p) {
    /* scan over word */
    while(*p && !isspace(*p)) *q++ = *p++;
    /* smash spaces */
    while(*p && isspace(*++p));
    if(*p) *q++ = ' '; /* add a space to separate next word */
  }
  /* terminate */
  *q = '\0';

  /* block dump to prevent db inconsistencies from showing up */
  alarm_block = 1;

  /* check for single-character commands */
  if(*command == SAY_TOKEN)
    do_say(player, command+1, NULL);
  else if(*command == POSE_TOKEN)
    do_pose(player, command+1, NULL);
    /* parse commands */

  /* new feature, parse @ commands before exits */
  else if(*command==COMMAND_TOKEN) {
    command++;
    /* parse arguments - warning, code repeated later (shrug) */
    for(arg1 = command; *arg1 && !isspace(*arg1); arg1++);
    if(*arg1) *arg1++ = '\0';
    while(*arg1 && isspace(*arg1)) arg1++;
    for(arg2 = arg1; *arg2 && *arg2 != ARG_DELIMITER; arg2++);
    for(p = arg2 - 1; p >= arg1 && isspace(*p); p--) *p = '\0';
    if(*arg2) *arg2++ = '\0';
    while(*arg2 && isspace(*arg2)) arg2++;
    
    {
      if(0) ; /* set up for Matched(), which is an else if */

    /* information commands */
      Matched("date", 2)	do_date(player, arg1, arg2);
      Matched("stats", 2)	do_stats(player, arg1);
      Matched("entrances", 2)	do_entrances(player, arg1);
      Matched("exits", 2)	do_exits(player, arg1);
      Matched("find", 2)	do_find(player, arg1);
#ifdef VERSION
      Matched("version", 1)	notify(player, VERSION);
#endif
      
    /* utility commands */
      Matched("name", 2)	do_name(player, arg1, arg2);
      Matched("describe", 2)	do_describe(player, arg1, arg2);
      Matched("set", 2)		do_set(player, arg1, arg2);
      Matched("examine", 2)	do_examine(player, arg1);
      Matched("fail", 2)	do_fail(player, arg1, arg2);
      Matched("ofail", 2)	do_ofail(player, arg1, arg2);
      Matched("success", 2)	do_success(player, arg1, arg2);
      Matched("osuccess", 2)	do_osuccess(player, arg1, arg2);
      Matched("link", 2)	do_link(player, arg1, arg2);
      Matched("unlink", 4)	do_unlink(player, arg1);
      Matched("lock", 2)	do_lock(player, arg1, arg2);
      Matched("unlock", 4)	do_unlock(player, arg1);
      Matched("password", 2)	do_password(player, arg1, arg2);
      Matched("teleport", 2)	do_teleport(player, arg1, arg2);
      
      /* building commands */
      Matched("create", 2)	do_create(player, arg1, atol(arg2));
      Matched("dig", 2)		do_dig(player, arg1);
      Matched("open", 2)	do_open(player, arg1, arg2);
      
#ifdef RECYCLE
      /* recycling commands */
      Matched("count", 2)	do_count(player, arg1);
      Matched("purge", 5)	do_purge(player, arg1, arg2);
      Matched("recycle", 1)	do_recycle(player, arg1);
#endif /* RECYCLE */
      
      /* wizard commands */
      Matched("gripe", 2)	do_gripe(player, arg1, arg2);
      Matched("wall", 4)	do_wall(player, arg1, arg2);
      Matched("chown", 2)	do_chown(player, arg1, arg2);
      Matched("owned", 2)	do_owned(player, arg1, arg2);
      Matched("top", 3)		do_top(player, arg1, arg2);
      Matched("force", 2)	do_force(player, arg1, arg2);
      Matched("boot", 4)	do_boot(player, arg1);
      Matched("dump", 4)	do_dump(player);
      Matched("shutdown", 8)	do_shutdown(player);
      Matched("tune", 4)	do_tune(player, arg1, arg2);
#ifdef TINKER
      Matched("bobble", 6)	do_bobble(player, arg1, arg2);
      Matched("unbobble", 3)	do_unbobble(player, arg1, arg2);
#else
      Matched("toad", 4)	do_bobble(player, arg1, arg2);
      Matched("untoad", 3)	do_unbobble(player, arg1, arg2);
#endif
      Matched("pcreate", 2)	do_pcreate(player, arg1, arg2);
      Matched("newpassword", 2)	do_newpassword(player, arg1, arg2);
      Matched("mass_teleport", 4)
	do_mass_teleport(player, arg1);

      /* no commands matched */
      else notify(player, "Huh?  (Type \"help\" for help.)");

    }
  } else if(can_move(player, command)) {
    /* command is an exact match for an exit */
    do_move(player, command);

  } else { /* ok, try the list of regular commands */

    /* parse arguments - warning, code repeated earlier */
    for(arg1 = command; *arg1 && !isspace(*arg1); arg1++);
    if(*arg1) *arg1++ = '\0';
    while(*arg1 && isspace(*arg1)) arg1++;
    for(arg2 = arg1; *arg2 && *arg2 != ARG_DELIMITER; arg2++);
    for(p = arg2 - 1; p >= arg1 && isspace(*p); p--) *p = '\0';
    if(*arg2) *arg2++ = '\0';
    while(*arg2 && isspace(*arg2)) arg2++;
    
    { 
      if(0) ; /* set up for Matched, which is an else if */

      /* real commands */
      Matched("look", 1)	do_look_at(player, arg1);
      Matched("inventory", 1)	do_inventory(player);
      Matched("examine", 2)	do_examine(player, arg1);

      Matched("page", 1)	do_page(player, arg1, arg2);
      Matched("say", 2)		do_say(player, arg1, arg2);
      Matched("whisper", 1)	do_whisper(player, arg1, arg2);

      Matched("get", 2)		do_get(player, arg1);
      Matched("take", 2)	do_get(player, arg1);
      Matched("drop", 1)	do_drop(player, arg1);
      Matched("throw", 2)	do_drop(player, arg1);

      Matched("give", 2)	do_give(player, arg1, atol(arg2));
      Matched("goto", 2)	do_move(player, arg1);
      Matched("move", 1)	do_move(player, arg1);
      Matched("help", 1)	do_help(player, arg1);
      Matched("news", 2)	do_news(player);
      Matched("read", 1)	do_look_at(player, arg1);
      Matched("kill", 1)	do_kill(player, arg1, atol(arg2));
      Matched("rob", 2)		do_rob(player, arg1);
      Matched("gripe", 2)	do_gripe(player, arg1, arg2);
      Matched("score", 2)	do_score(player);

      else { /* no commands matched */
	notify(player, "Huh?  (Type \"help\" for help.)");

#ifdef LOG_FAILED_COMMANDS
	if(!controls(player, db[player].location)) {
	  writelog("HUH from %s(%d) in %s(%d)[%s]: %s %s\n",
		   db[player].name, player,
		   db[db[player].location].name,
		   db[player].location,
		   db[db[db[player].location].owner].name,
		   command,
		   reconstruct_message(arg1, arg2));
	}
#endif /* LOG_FAILED_COMMANDS */
      }
    }
  }

  alarm_block = 0;
  if(alarm_triggered) fork_and_dump();
}
#undef Matched
