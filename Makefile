#################################################################
#
# Makefile for TinyMUD source code... 15 November 1990
#
#################################################################
#
# Whatever you put in for $(CC) must be able to grok ANSI C.
#

# GCC:
CC=gcc
OPTIM= -g -W -Wreturn-type -Wunused -Wcomment -Wwrite-strings -m32

# Systems with 'cc' built from GCC (IBM RT, NeXT):
#CC=cc
#OPTIM=-g

# Dec 3100 C compiler
#CC=cc
#OPTIM= -g -Dconst=

# Directories for 'make install'
BINDIR= $(HOME)/mud/bin
LIBDIR= $(HOME)/mud/lib
# Note that there is a subdirectory help/. 'make install' will create
# a link to this directory in the LIBDIR. use 'restart', or cd into
# the libdir before running netmud when you use 'make install.' If
# help/ does not exist, the help command uses HELP_FILE instead

# Compile time options for game
# 
# LOG_COMMANDS		log all commands given by anyone to log file
# LOG_FAILED_COMMANDS	log failed commands (HUH's) to log file
# RESTRICTED_BUILDING	restrict object-creation to players with
#			the BUILDER bit set
# USE_VFORK		forces fork_and_dump to use vfork() instead of fork()
# DBB_DOUBLING		forces grow_database() to be clever about its memory
#			management. Use this only if your realloc does not
#			allocate in powers of 2 (if you already have a clever
#			realloc, this option will only cost you extra space).
# DBB_INITIAL_SIZE=xxx	allocate initial space for xxx objects (minimum 1)
#			instead of the default of 10000. Space grows as needed.
# TEST_MALLOC		include code to keep track of # of blocks allocated.
# COMPRESS		include code to compress string data
# QUIET_WHISPER		eliminate public message when a player whispers
# GENDER		includes Stephen White's gender flags and
#			pronoun substitution code. (%n, %s, %o, %p)
# REGISTRATION		to disable login-time creation of players
# HOST_NAME		have logs and WHO use hostnames instead of addresses
# GOD_PRIV		limit @set wizard and WHO privs to GOD (usually #1)
# 			when GOD_PRIV is set, 3 other options are meaningful:
#   GOD_ONLY_HOSTS	only gods will see hostnames in wholist
#   GOD_ONLY_PCREATE	to restrict @pcreate to Gods (see REGISTRATION)
#   POLYTHEISM		to use the GOD flag to extend #1 power to players.
#			Gods can turn the WIZARD flag on and off at will.
# CONNECT_MESSAGES	have messages for connect and disconnect
# PLAYER_LIST		to use a hashed player list for player name lookups
# DETACH		causes netmud to detach itself from the terminal on
#			startup. stderr becomes LOG_FILE, set in config.h.
# RECYCLE		adds the @count and @recycle commands
# NODUMPCORE		to disable core dump on errors
# ROBOT_MODE		adds the ROBOT flag (allowing robots to be excluded
#			from rooms which are set ROBOT).
# TINKER		to use @BOBBLE, TINKER, JUNKPILE, and 'donate' instead
#			of @TOAD, WIZARD, and TEMPLE, and 'sacrifice'
# NOFAKES		to prevent users from using confusing names
#			(like A, An, The, You, Your, Going, Huh?)
# TIMESTAMPS		to mark all things with a timestamp and usecount
# RESTRICTED_TELEPORT	allow only wizards to teleport
# LOOSE_TELEPORT	allow extended player use of teleporting
# CONTROL_ENTRANCES	give player control of entrances to room
# ILLUMINATI		enable special K (CLUE) flag
# IRIX                  for stupid computers who reset signal() to SIG_DFL when
#                       called (IRIX, and possibly other Operating Systems)

DEFS=	-DTEST_MALLOC -DCOMPRESS -DQUIET_WHISPER -DGENDER -DHOST_NAME \
	-DGOD_PRIV -DCONNECT_MESSAGES -DPLAYER_LIST -DDETACH -DRECYCLE \
	-DROBOT_MODE -DNOFAKES -DTIMESTAMPS -DLOOSE_TELEPORT \
	-DCONTROL_ENTRANCES

CFLAGS= $(OPTIM) $(DEFS)

# Header files
HFILES= config.h copyright.h db.h externs.h interface.h match.h

# Everything needed to use db.c
DBFILES= db.c compress.c player_list.c stringutil.c
DBOFILES= db.o compress.o player_list.o stringutil.o

# Files used by the programs
CFILES= boolexp.c create.c game.c help.c look.c match.c move.c \
	player.c predicates.c rob.c set.c speech.c unparse.c \
	utils.c wiz.c $(DBFILES)

# .o versions of above
OFILES= boolexp.o create.o game.o help.o look.o match.o move.o \
	player.o predicates.o rob.o set.o speech.o unparse.o \
	utils.o wiz.o $(DBOFILES)

# Files in the standard distribution
DISTFILES= $(HFILES) $(CFILES) \
	conc.c interface.c oldinterface.c \
	decompress.c dump.c extract.c sanity-check.c \
	restart do_gripes \
	connect.txt help.txt \
	small.db small.db.README minimal.db tinybase.db \
 	README CHANGES Makefile

OUTFILES= concentrate netmud.conc netmud \
	  decompress dump extract sanity-check

all: $(OUTFILES) tags

tags: *.c *.h
	etags *.c *.h

netmud.conc: $P interface.o $(OFILES)
	-mv -f netmud.conc netmud.conc~
	$(CC) $(CFLAGS) -o netmud.conc interface.o $(OFILES)

netmud: $P oldinterface.o $(OFILES)
	-mv -f netmud netmud~
	$(CC) $(CFLAGS) -o netmud oldinterface.o $(OFILES)

concentrate: $P conc.o config.h
	-mv -f concentrate concentrate~
	$(CC) $(CFLAGS) -o concentrate conc.o

decompress: $P decompress.o compress.o
	-rm -f decompress
	$(CC) $(CFLAGS) -o decompress decompress.o compress.o

dump: $P dump.o unparse.o $(DBOFILES)
	-rm -f dump
	$(CC) $(CFLAGS) -o dump dump.o unparse.o $(DBOFILES) 

extract: $P extract.o utils.o $(DBOFILES) 
	-rm -f extract
	$(CC) $(CFLAGS) -o extract extract.o utils.o $(DBOFILES)

sanity-check: $P sanity-check.o utils.o $(DBOFILES) 
	-rm -f sanity-check
	$(CC) $(CFLAGS) -o sanity-check sanity-check.o utils.o $(DBOFILES)

clean:
	-rm -f *.o a.out core gmon.out $(OUTFILES)

dist:	$(DISTFILES)
	tar cvf - $(DISTFILES) help | compress -c > dist.tar.Z.NEW
	mv dist.tar.Z.NEW dist.tar.Z

install: $(OUTFILES)
	cp restart $(OUTFILES) $(BINDIR)
	cp *.txt $(LIBDIR)
	ln -s `pwd`/help $(LIBDIR)/help

# DO NOT REMOVE THIS LINE OR CHANGE ANYTHING AFTER IT #
compress.o: compress.c copyright.h
db.o: db.c db.h config.h copyright.h
player_list.o: player_list.c copyright.h db.h config.h interface.h externs.h
stringutil.o: stringutil.c copyright.h externs.h

boolexp.o: boolexp.c $(HFILES)
create.o: create.c $(HFILES)
game.o: game.c $(HFILES)
help.o: help.c $(HFILES)
look.o: look.c $(HFILES)
match.o: match.c $(HFILES)
move.o: move.c $(HFILES)
player.o: player.c $(HFILES)
predicates.o: predicates.c $(HFILES)
rob.o: rob.c $(HFILES)
set.o: set.c $(HFILES)
speech.o: speech.c $(HFILES)
unparse.o: unparse.c $(HFILES)
utils.o: utils.c $(HFILES)
wiz.o: wiz.c $(HFILES)

conc.o: conc.c config.h
interface.o: interface.c db.h interface.h config.h
oldinterface.o: oldinterface.c db.h interface.h config.h

decompress.o: decompress.c
dump.o: dump.c db.h
extract.o: extract.c db.h
sanity-check.o: sanity-check.c db.h config.h

config.h: copyright.h
db.h: copyright.h
externs.h: copyright.h db.h
interface.h: copyright.h db.h
match.h: copyright.h db.h
copyright.h:
