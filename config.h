#include "copyright.h"
#define VERSION		"TinyMUD Version 1.5.5, 15 November 1990"

/* room number of player start location */
#define PLAYER_START ((dbref) 0)
/* someone to send gripes to at the very least */
#define GOD_DBREF ((dbref) 1)

/* minimum cost to create various things */
#define OBJECT_COST 10
#define EXIT_COST 1
#define LINK_COST 1
#define ROOM_COST 10

/* cost for @find, @owned, and @count */
#define FIND_COST 100

/* cost for pages */
#define PAGE_COST 1

/* limit on player name length */
#define PLAYER_NAME_LIMIT 24

/* amount of object endowment, based on cost */
#define MAX_OBJECT_ENDOWMENT 100
#define OBJECT_ENDOWMENT(cost) (((cost)-5)/5)
#define PAYBACK(value) (((value)*5)+5)

/* amount at which temple stops being so profitable */
#define MAX_PENNIES 10000

/* penny generation parameters */
#define PENNY_RATE 10		/* 1/chance of getting a penny per room */

/* costs of kill command */
#define KILL_BASE_COST 100	/* prob = expenditure/KILL_BASE_COST */
#define KILL_MIN_COST 10
#define KILL_BONUS 50		/* paid to victim */

/* timing stuff */
# define DUMP_INTERVAL 10800   /* 3 hours between dumps */

#define COMMAND_TIME_MSEC  1000	/* time slice length in milliseconds */
#define COMMAND_BURST_SIZE   20	/* commands allowed per user in a burst */
#define COMMANDS_PER_TIME     1	/* commands per time slice after burst */
#define COMMAND_ALLOW_EXTRA   1 /* Allow commands over quota if not busy */

/* maximum amount of queued output */
#define MAX_OUTPUT 32768

/* default ports */
#define TINYPORT 2323
#define INTERNAL_PORT 2322

#define QUIT_COMMAND "QUIT"
#define WHO_COMMAND "WHO"
#define PREFIX_COMMAND "OUTPUTPREFIX"
#define SUFFIX_COMMAND "OUTPUTSUFFIX"

#define WELCOME_MESSAGE "<connected>\n"
#define LEAVE_MESSAGE "<disconnected>\n"
#define FLUSHED_MESSAGE "<output flushed>\n"
#define SHUTDOWN_MESSAGE "<going down - bye>\n"

/* none of the following files need to exist, but HELP_FILE really should */
#define LOG_FILE	"tinymud.log"   /* default file to log to */
#define CONNECT_FILE    "connect.txt"   /* printed before they connect */
#define MOTD_FILE	"motd.txt"      /* printed when they connect */
#ifdef TINKER
#define WIZARD_FILE	"tinker.txt"    /* printed for tinkers only */
#else
#define WIZARD_FILE     "wizard.txt"    /* printed for wizards only */
#endif
#define DISCONNECT_FILE "disconnect.txt" /* printed when they QUIT */

#define REGISTER_FILE	"register.txt"	/* info on how to register */
#define HELP_FILE	"help.txt"      /* for help command */
#define NEWS_FILE	"news.txt"      /* for news command */

/* magic cookies */
#define NOT_TOKEN '!'
#define AND_TOKEN '&'
#define OR_TOKEN '|'

#define LOOKUP_TOKEN '*'
#define NUMBER_TOKEN '#'
#define ARG_DELIMITER '='
#define EXIT_DELIMITER ';'

/* magic command cookies */
/* none of these are subject to prior exit checking */
#define SAY_TOKEN '"'
#define POSE_TOKEN ':'
#define COMMAND_TOKEN '@'

/* flag defs for unparse.c */

# define TYPE_CODES	"R-EP" /* Room, thing, exit, player */
# define STICKY_MARK	'S'
# define DARK_MARK	'D'
# define LINK_MARK	'L'
# define ABODE_MARK	'A'
# define HAVEN_MARK	'H'

#ifdef GENDER
# define UNWANTED_MARK	'U'
# define MALE_MARK	'M'
# define FEMALE_MARK	'F'
# define NEUTER_MARK	'N'
#endif

#ifdef ILLUMINATI
#  define CLUED_MARK     'K'
#endif

#ifdef POLYTHEISM
#  define GOD_MARK	'G'  /* For Gods */
#endif

#define QUELL_MARK	'Q'  /* Quell wizard powers */

#ifdef ROBOT_MODE
#  define ROBOT_MARK	'B'  /* For Bot */
#endif

#ifdef RESTRICTED_BUILDING
#  define BUILDER_MARK	'C'  /* For Constructor/Creator */
#endif

#ifndef TINKER
# define WIZARD_MARK	'W'  /* For Wizard */
# define TEMPLE_MARK	'T'  /* For Temple */
#else
# define WIZARD_MARK	'T'  /* For Tinker */
# define TEMPLE_MARK	'J'  /* For Junkpile */
#endif

/* For future reference, the following flags are unimplemented: */
/* I O V X Y Z */
