#include "copyright.h"
#include <ctype.h>
#include <string.h>

/* commands for giving help */

#include "db.h"
#include "config.h"
#include "interface.h"
#include "externs.h"

int spit_file(dbref player, const char *filename)
{
    FILE *f;
    char buf[BUFFER_LEN];
    char *p;

    if((f = fopen(filename, "r")) == NULL) {
	return (0);
    } else {
	while(fgets(buf, sizeof buf, f)) {
	    for(p = buf; *p; p++) if(*p == '\n') {
		*p = '\0';
		break;
	    }
	    notify(player, buf);
	}
	fclose(f);
	return (1);
    }
}

void do_help(dbref player, char *subject)
{
    char *p, buf[64];

    if(*subject) { /* player wants help on specific subject */
      if(strlen(subject)>32) {
	  notify(player, "Sorry, that subject is too long.");
	  return;
      }
      p = subject;
      while(*p)
	if(isalnum(*p) || (ispunct(*p) && !strchr("~/.", *p))) {
	    *p = (isupper(*p) ? tolower(*p) : *p);
	    p++;
	} else {
	    notify(player, "Sorry, that is not a reasonable subject.");
	    return;
	}
      sprintf(buf, "help/%s", subject);
      if (!spit_file(player, buf))
	notify(player, "Sorry, no help is available on that subject.");
  } else if (!spit_file(player, "help/HELP"))
    if(!spit_file(player, HELP_FILE)) {
	notify(player, "Sorry, the help file is missing right now.");
	do_gripe(player, "automatic gripe: no help file!", HELP_FILE);
    }
}

void do_news(dbref player)
{ int result = 0;

  result += spit_file(player, NEWS_FILE);
  result += spit_file(player, MOTD_FILE);
  if (Wizard(player)) result += spit_file(player, WIZARD_FILE);

  if (result == 0)
  { notify(player, "No news today."); }
}

void do_motd(dbref player)
{
    spit_file(player, MOTD_FILE);
    if (Wizard(player)) spit_file(player, WIZARD_FILE);
}
