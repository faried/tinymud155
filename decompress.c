#include <stdio.h>

const char *uncompress(const char *s);

int main()
{
    char buf[16384];

    while(gets(buf)) {
	puts(uncompress(buf));
    }
    return 0;
}

#ifndef COMPRESS
const char *uncompress(const char *s)
{
	return s;
}
#endif
