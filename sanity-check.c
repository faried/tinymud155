#include "copyright.h"
  
#include <stdio.h>
#include <stdlib.h>
  
#include "db.h"
#include "config.h"
#include "externs.h"

  /* object probably shouldn't contain more than this many exits or objects */
#define RECURSIVE_COUNT 10000

  /* make sure this flag doesn't conflict any in db.h ! */
#define OBJECT_SEEN 0x40000000

#define NotObject(thing) ((thing) < 0 || (thing) >= db_top) 
int unlinked_exits = 0;
int carried_exits = 0;
int home_exits = 0;
int dead_rooms = 0;
int unreachable_rooms = 0;

FILE *recfile;

void check_destination(dbref i)
{
    dbref dest;
    
    dest = db[i].location;
    
    if(dest==NOTHING) {
	if(recfile) fprintf(recfile, "%d\n", i);
	unlinked_exits++;
    } else if(dest==HOME) {
      home_exits++;
    } else if(NotObject(dest)) {
      printf("%d has out of range destination %d\n", i, dest);
    } else if(Typeof(dest) == TYPE_PLAYER) {
      if(!member(i, db[dest].contents))
	printf("%d is linked to player %d\n", i, dest);
    } else if(Typeof(dest) != TYPE_ROOM) {
	printf("%d has bad destination type %d\n", i, dest);
    } else
	db[dest].flags |= OBJECT_SEEN;
}

void check_exits(dbref i)
{
    dbref exit;
    int count;
    
    count = RECURSIVE_COUNT;
    DOLIST(exit, db[i].exits) {
	if(NotObject(exit)) {
	    printf("%d has out of range exit %d\n", i, exit);
	    break;
	} else if(Typeof(exit) != TYPE_EXIT) {
	    printf("%d has bad exit %d\n", i, exit);
	}
	db[exit].flags |= OBJECT_SEEN; /* for later search */
	
	if(count-- < 0) {
	    printf("%d has looping exits\n", i);
	    break;
	}
    }
}

void check_inventory(dbref i)
{
    dbref thing;
    dbref loc;
    int count;
    
    count = RECURSIVE_COUNT;
    DOLIST(thing, db[i].contents) {
	if(NotObject(thing)) {
	    printf("%d holding out of range object %d\n", i, thing);
	    break;
	} else if(Typeof(thing)==TYPE_THING || Typeof(thing)==TYPE_EXIT) {
	    if((loc = db[thing].location) != i)
	      printf("%d in %d but location is %d\n", thing, i, loc);
	    if(Typeof(thing)==TYPE_EXIT) { 
		carried_exits++;
		db[thing].flags |= OBJECT_SEEN;
	    }
	} else {
	    printf("%d holding bad object %d\n", i, thing);
	}

	if(count-- < 0) {
	    printf("%d has looping inventory\n", i);
	    break;
	}
    }
}

void check_contents(dbref i)
{
    dbref thing;
    dbref loc;
    int count;
    
    count = RECURSIVE_COUNT;
    DOLIST(thing, db[i].contents) {
	if(NotObject(thing)) {
	    printf("%d contains out of range object %d\n", i, thing);
	    break;
	} else if(Typeof(thing)!=TYPE_THING && Typeof(thing)!=TYPE_PLAYER) {
	    printf("%d contains bad object %d\n", i, thing);
	} else if((loc = db[thing].location) != i) {
	    printf("%d in %d but location is %d\n", thing, i, loc);
	}

	if(count-- < 0) {
	    printf("%d has looping contents\n", i);
	    break;
	}
    }
}

void check_location(dbref i)
{
    dbref loc;
    
    loc = db[i].location;
    if(NotObject(loc)) {
	printf("%d has out of range location %d\n", i, loc);
    } else if(!member(i, db[loc].contents)) {
	printf("%d not in loc %d\n", i, loc);
    }
}

void check_owner(dbref i)
{
    dbref owner;
    
    owner = db[i].owner;
    if(NotObject(owner)) {
	printf("%d has out of range owner %d\n", i, owner);
    } else if(Typeof(owner) != TYPE_PLAYER) {
	printf("%d has bad type owner %d\n", i, owner);
    }
}

void check_pennies(dbref i)
{
    dbref pennies;
    
    pennies = db[i].pennies;
    
    switch(Typeof(i)) {
      case TYPE_PLAYER:
	if(pennies < 0 || pennies > MAX_PENNIES)
	    printf("Player %d has %d pennies\n", i, pennies);
	break;
      case TYPE_THING:
	if(pennies < 0 || pennies > MAX_OBJECT_ENDOWMENT)
	    printf("Object %d endowed with %d pennies\n", i, pennies);
	break;
      case TYPE_EXIT:
      case TYPE_ROOM:
	if(pennies != 0)
	    printf("Odd type object %d has %d pennies\n", i, pennies);
	break;
    }
}

int main(int argc, char **argv)
{
    dbref i;
    int garbage = 0;
    
    if(db_read(stdin) < 0) {
	puts("Database load failed!");
	exit(1);
    } 
    puts("Done loading database");
    
    if(argc>1)
      if((recfile=fopen(argv[1], "w")) == NULL)
	printf("error opening recycle file %s\n", argv[1]);
      else printf("opened recycle file %s\n", argv[1]);
    else puts("No recycle file.");

    for(i = 0; i < db_top; i++) {
	if(Typeof(i)==TYPE_GARBAGE)
	  garbage++;
	else {
	    check_owner(i);
	    check_pennies(i);
	    switch(Typeof(i)) {
	      case TYPE_EXIT:
		check_destination(i);
		break;
	      case TYPE_PLAYER:
		check_location(i);
		check_inventory(i);
		if(God(i)) printf("God: %s(%d)\n", db[i].name, i);
		if(Wizard(i)) printf("Wizard: %s(%d)\n", db[i].name, i);
		if(Dark(i)) printf("Dark: %s(%d)\n", db[i].name, i);
		if(Robot(i)) printf("Robot: %s(%d)\n", db[i].name, i);
		if(db[i].password == NULL)
		  { printf ("Null password: %s(%d)\n", db[i].name, i); }
		break;
	      case TYPE_THING:
		check_location(i);
		break;
	      case TYPE_ROOM:
		if(Robot(i)) printf("AntiRobot: %s(%d)\n", db[i].name, i);
		check_contents(i);
		check_exits(i);
		break;
	    }
	}
    }
    
    /* scan for unmarked exits and rooms */
    for(i = 0; i < db_top; i++) {
	if(Typeof(i)==TYPE_EXIT) {
	    if(!Flag(i, OBJECT_SEEN))
	      printf("Exit %d has no source\n", i);
	} else if (Typeof(i)==TYPE_ROOM) {
	    if(!Flag(i, OBJECT_SEEN)) {
		if(db[i].exits == NOTHING && db[i].contents == NOTHING) {
		    if(recfile) fprintf(recfile, "%d\n", i);
		    /* rooms with no entrances, exits, or contents */
		    dead_rooms++;
		} else {
		    printf("Room %d has no entrances\n", i);
		    unreachable_rooms++;
		}
	    }
	}
    }
    
    printf("Totals:\n");
    printf("%d garbage\n", garbage);
    printf("%d exits being carried\n", carried_exits);
    printf("%d exits linked to HOME\n", home_exits);
    printf("%d unreachable rooms\n", unreachable_rooms);
    if(recfile) {
	printf("%d unlinked exits written to recycle file\n", unlinked_exits);
	printf("%d dead rooms written to recycle file\n", dead_rooms);
    } else {
	printf("%d unlinked exits\n", unlinked_exits);
	printf("%d empty rooms with no exits or entrances\n", dead_rooms);
    }
    if(recfile) fclose(recfile);
    exit(0);
}
